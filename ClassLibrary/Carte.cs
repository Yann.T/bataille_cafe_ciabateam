﻿using System.Collections.Generic;

namespace ClassLibrary
{   /// <summary>
    /// Cette classe représente le plateau de jeu composé de parcelles et d'unités.
    /// Elle permet d'initaliser le tableau d'Unités et la liste des parcelles.
    /// </summary>
    public class Carte
    {
        public List<Parcelle> Parcelles { get; } = new List<Parcelle>();
        public Unite[,] Unites { get; }
        public byte[] DernierePose { get; set; } = new byte[2];

        /// <summary>
        /// Constructeur de la carte
        /// </summary>
        public Carte() => Unites = new Unite[10, 10];

        /// <summary>
        /// Retourne le tableau d'unités
        /// </summary>
        /// <param name="p_coordonneX">Coordonnée horizontale de l'unite à récuperer</param>
        /// <param name="p_coordonneY">Coordonnée verticale de l'unite à récuperer</param>
        /// <returns>Instance d'une unite de la case fournie en paramétre</returns>
        public Unite GetUnite(byte p_coordonneX, byte p_coordonneY) => Unites[p_coordonneX, p_coordonneY];

        /// <summary>
        /// Ajout de parcelle à la carte
        /// </summary>
        /// <param name="p_parcelle">Instance d'une Parcelle</param>
        public void AddParcelle(Parcelle p_parcelle) => Parcelles.Add(p_parcelle);

        /// <summary>
        /// Ajout d'une unité dans le tableau d'unités
        /// </summary>
        /// <param name="uneUnite">Instance d'une Unite</param>
        /// <param name="p_coordonneX">Coordonnée horizontale de l'unite à récuperer</param>
        /// <param name="p_coordonneY">Coordonnée verticale de l'unite à récuperer</param>
        public void AjoutUnite(Unite uneUnite, byte p_coordonneX, byte p_coordonneY) => Unites[p_coordonneX, p_coordonneY] = uneUnite;

        /// <summary>
        /// Prise de contrôle d'une Cellule si toutes les conditions sont remplies
        /// </summary>
        /// <param name="p_coordonneX">Coordonnée horizontale de l'unite à capturer</param>
        /// <param name="p_coordonneY">Coordonnée verticale de l'unite à capturer</param>
        /// <param name="p_joueur">Instance du joueur souhaitant capturer la case</param>
        /// <returns>Retourne si oui ou non le pion a était placé</returns>
        public bool PriseControleCellule(byte p_coordonneX, byte p_coordonneY, Player p_joueur)
        {
            if (p_coordonneX < 10 && p_coordonneY < 10)
                if (!Unites[p_coordonneX, p_coordonneY].Occupied)
                {
                    Unites[p_coordonneX, p_coordonneY].Take(p_joueur);
                    return true;
                }
            return false;
        }

        /// <summary>
        /// Retourne la première parcelle de la liste possèdant le plus d'Unitées
        /// </summary>
        public Parcelle MeilleurParcelle
        {
            get
            {
                byte MeilleurTaille = 0;
                foreach (Parcelle parcelle in Parcelles)
                    MeilleurTaille = (parcelle.SizeParcelle > MeilleurTaille) ? parcelle.SizeParcelle : MeilleurTaille;
                return Parcelles.Find(x => x.SizeParcelle == MeilleurTaille);
            }
        }
    }
}