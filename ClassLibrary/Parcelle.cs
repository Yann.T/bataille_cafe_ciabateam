﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary
{
    public class Parcelle
    {
        public byte NumeroParcelle { get; }
        public List<Unite> Unites { get; } = new List<Unite>();
        public bool Occupied { get; private set; } = false;
        public Player Owner { get; private set; } = null;
        public byte SizeParcelle => (byte)Unites.Count();

        /// <summary>
        /// Constructeur de l'objet parcelle.
        /// </summary>
        /// <param name="p_unites"> Liste d'unités que la parcelle possède </param>
        /// <param name="p_numeroParcelle"> Numéro de cette nouvelle parcelle </param>
        public Parcelle(List<Unite> p_unites, byte p_numeroParcelle) // constructeur de l'objet parcelle
        {
            NumeroParcelle = p_numeroParcelle;
            Unites = p_unites;
        }

        #region Fonctions

        /// <summary>
        /// Changement en fonction des résultats de l'appartenance de la parcelle
        /// </summary>
        /// <param name="p_compteurJoueurUn"> int : nombre d'unités que le joueur 1 possède</param>
        /// <param name="p_compteurJoueurDeux"> int : nombre d'unités que le joueur 2 possède </param>
        /// <param name="p_joueur"> Player : sert à redéfinir le propriétaire de la parcelle</param>
        private void UpdateOwnerControl(byte p_compteurJoueurUn, byte p_compteurJoueurDeux, Player p_joueur) //fonction interne de UpdateOwner
        {
            if (p_compteurJoueurUn > p_compteurJoueurDeux && Owner.Id != 1) // vérifie si le joueur 1 devrait être le proprio mais ne l'est pas
                Owner = p_joueur;
            if (p_compteurJoueurUn < p_compteurJoueurDeux && Owner.Id != 2) // vérifie si le joueur 2 devrait être le proprio mais ne l'est pas
                Owner = p_joueur;
            if (p_compteurJoueurUn == p_compteurJoueurDeux) // personne n'a le contrôle de la parcelle
                Owner = null;
        }

        /// <summary>
        /// Actualisation de l'appartenance de la parcelle après chaque placement de graine.
        /// </summary>
        /// <param name="p_joueur">Sert à redéfinir le propriétaire de la parcelle</param>
        public void UpdateOwner(Player p_joueur)
        {
            if (Owner != null) // Vérifie si quelqu'un a déjà le contrôle de la parcelle
            {
                byte v_compteurJoueurUn = 0, v_compteurJoueurDeux = 0;
                foreach (Unite v_unite in Unites) // Permet de vérifier chaque case de la parcelle
                    if (v_unite.Owner != null)
                    {
                        v_compteurJoueurUn = (v_unite.Owner.Id == 1) ? ++v_compteurJoueurUn : v_compteurJoueurUn; // si unité possédée par J1, incrémenter compteur J1
                        v_compteurJoueurDeux = (v_unite.Owner.Id == 2) ? ++v_compteurJoueurDeux : v_compteurJoueurDeux; // si unité possédée par J2, incrémenter compteur J2
                    }
                UpdateOwnerControl(v_compteurJoueurUn, v_compteurJoueurDeux, p_joueur);
            }
            else
                Owner = p_joueur;
            Occupied = (Owner != null); //permets de changer si la parcelle est occupée
        }

        public bool HaveControl(Player p_player)
        {
            byte v_nombreCaseUser = 0, v_nombreCaseAdversaire = 0;
            foreach (Unite v_unite in Unites)
                _ = v_unite.Owner == null ? (byte)0 : (v_unite.Owner.Id == p_player.Id) ? v_nombreCaseUser++ : v_nombreCaseAdversaire++;
            return v_nombreCaseUser > v_nombreCaseAdversaire;
        }

        #endregion Fonctions
    }
}