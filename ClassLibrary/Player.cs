﻿namespace ClassLibrary
{
    public class Player
    {
        public byte Id { get; set; }
        public byte Points { get; set; }
        public byte NbGraines { get; set; } = 28;
        public byte TaillePlusGrandeParcelle { get; set; }

        /// <summary>
        /// Constructeur par défault de Player
        /// </summary>
        /// <param name="p_id">Numéro du joueur</param>
        public Player(byte p_id) => Id = p_id;

        /// <summary>
        /// Permet de prendre le contrôle de la case spécifiée dans la carte fournie
        /// </summary>
        /// <param name="p_coordonneX">Byte correspondant à la case horizontale</param>
        /// <param name="p_coordonneY">Byte correspondant à la case verticale</param>
        /// <param name="p_carte">Instance de l'objet carte</param>
        public void PosePion(byte p_coordonneX, byte p_coordonneY, Carte p_carte)
        {
            if (p_carte.PriseControleCellule(p_coordonneX, p_coordonneY, this))
                NbGraines--;
        }
    }
}