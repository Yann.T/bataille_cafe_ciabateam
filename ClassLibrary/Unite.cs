﻿namespace ClassLibrary
{
    public class Unite
    {
        public byte CoordonneeX { get; }
        public byte CoordonneeY { get; }
        public bool Occupied { get; set; } = false;
        public Player Owner { get; private set; } = null;
        public char Type { get; set; }
        public Parcelle Parcelle { get; set; }

        private bool[] m_frontieres = { false, false, false, false };
        public bool IsTerre => Type == 'T';
        public bool IsUsuable => IsTerre && !Occupied;

        /// <summary>
        /// Constructeur de l'Unite, représantant une cellule de la carte
        /// </summary>
        /// <param name="p_coordonneeX">coordonnée horizontale de la cellule</param>
        /// <param name="p_coordonneeY">coordonnée verticale de la cellule</param>
        public Unite(byte p_coordonneeX, byte p_coordonneeY)
        {
            CoordonneeX = p_coordonneeX;
            CoordonneeY = p_coordonneeY;
        }

        #region Getter Setter

        /// <summary>
        /// Retourne une case du tableau qui représente les 4 frontières d'une unité
        /// </summary>
        /// <param name="p_index">L'index de la case (va de 0 à 3)</param>
        /// <returns></returns>
        public bool GetFrontieres(byte p_index) => m_frontieres[p_index];

        /// <summary>
        /// Permet de définir la frontière de l'unité en fonction du numéro fourni.
        /// </summary>
        /// <param name="p_side"></param>
        /// <param name="p_exists"></param>
        public void SetFrontieres(byte p_side, bool p_exists) => m_frontieres[p_side] = p_exists;

        /// <summary>
        /// Méthode à appeler quand un joueur prend cette unité
        /// </summary>
        /// <param name="p_player">Référence vers l'instance d'objet Player qui souhaite prendre l'unité</param>
        public void Take(Player p_player)
        {
            ControleLimite(p_player);
            Parcelle.UpdateOwner(p_player);
        }

        /// <summary>
        /// Méthode pour prendre le contrôle d'une unité sans modifié l'état de la parcelle
        /// </summary>
        /// <param name="p_player">Référence vers l'instance d'objet Player qui souhaite prendre l'unité</param>
        public void ControleLimite(Player p_player)
        {
            Occupied = true;
            Owner = p_player;
        }

        /// <summary>
        /// Méthode pour prendre le contrôle d'une unité sans modifié l'état de la parcelle
        /// </summary>
        public void ControleLibere()
        {
            Occupied = false;
            Owner = null;
        }

        #endregion Getter Setter
    }
}