﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.IAObject
{
    public static class CalculateurDePoints
    {
        private static bool[,] PlacementPossible;
        private static byte[] m_dernierePosition;
        private static Carte m_carte;
        private static Player m_joueur;
        private static Player m_adversaire;
        private static byte m_ptsAdv;
        private static List<byte> ListHori => InitListPlacementHorizontal();
        private static List<byte> ListVerti => InitListPlacementVertical();
        private static byte MaxPointListHori => ListHori.Max();
        private static byte MaxPointListVerti => ListVerti.Max();

        private static byte[] CoordPointVerti(byte indice) => new byte[] { m_dernierePosition[0], indice };

        private static byte[] CoordPointHori(byte indice) => new byte[] { indice, m_dernierePosition[1] };

        private static bool VerifParcelle(byte p_xSuivant, byte p_ySuivant)
        {
            //Vérification de si c'est bien une parcelle terre
            if (m_carte.GetUnite(p_xSuivant, p_ySuivant).Parcelle == null) return false;
            //Vérification que la graine ne sois pas posé dans la même parcelle
            byte v_idParcelle = m_carte.GetUnite(m_dernierePosition[0], m_dernierePosition[1]).Parcelle.NumeroParcelle;
            byte v_idParcellePositionSuivante = m_carte.GetUnite(p_xSuivant, p_ySuivant).Parcelle.NumeroParcelle;
            return v_idParcelle != v_idParcellePositionSuivante;
        }

        /// <summary>
        /// Initialise la carte de pour autorisé ou non les placements
        /// </summary>
        private static void InitPlacementPossible()
        {
            PlacementPossible = new bool[10, 10];
            for (byte indiceX = 0; indiceX < 10; indiceX++)
                for (byte indiceY = 0; indiceY < 10; indiceY++)
                    PlacementPossible[indiceX, indiceY] = VerifParcelle(indiceX, indiceY);
        }

        /// <summary>
        /// Initialisation de la classe à appeler obligatoirement avant d'utilisé les autres fonctions
        /// </summary>
        /// <param name="p_dernierePosition"></param>
        /// <param name="p_carte"></param>
        public static void InitClass(byte[] p_dernierePosition, Carte p_carte, Player p_joueur, Player p_adversaire)
        {
            m_dernierePosition = p_dernierePosition;
            m_carte = p_carte;
            InitPlacementPossible();
            m_joueur = p_joueur;
            m_adversaire = p_adversaire;
        }

        /// <summary>
        /// Fournit la liste de tout les résultats possible de pose pour tout les mouvements verticaux
        /// </summary>
        /// <returns>Liste de byte possèdant tout les résultats de pose possible</returns>
        private static List<byte> InitListPlacementVertical()
        {
            List<byte> v_listPoints = new List<byte>();
            for (byte v_indice = 0; v_indice < 10; v_indice++)
                v_listPoints.Add((v_indice != m_dernierePosition[1]) ? PlacementPossible[m_dernierePosition[0], v_indice] //Verification de si oui ou non la case est possible
                   ? ComptaPointCase.ComptabilisationPlacementFutur(m_carte, m_joueur, m_dernierePosition[0], v_indice, m_ptsAdv, m_adversaire) //Calcul du résultat de la case
                   : (byte)0 : (byte)0); //Case impossible à poser
            return v_listPoints;
        }

        /// <summary>
        /// Fournit la liste de tout les résultats possible de pose pour tout les mouvements Horizontaux 
        /// </summary>
        /// <returns>Liste de byte possèdant tout les résultats de pose possible</returns>
        private static List<byte> InitListPlacementHorizontal()
        {
            List<byte> v_listPoints = new List<byte>();
            for (byte v_indice = 0; v_indice < 10; v_indice++)
                v_listPoints.Add((v_indice != m_dernierePosition[1]) ? PlacementPossible[v_indice, m_dernierePosition[1]] //Verification de si oui ou non la case est possible
                    ? ComptaPointCase.ComptabilisationPlacementFutur(m_carte, m_joueur, v_indice, m_dernierePosition[1], m_ptsAdv, m_adversaire) //Calcul du résultat de la case
                    : (byte)0 : (byte)0); //Case impossible à poser
            return v_listPoints;
        }

        /// <summary>
        /// Retourne la meilleure coordonnée de placement par rapport au listes membres
        /// </summary>
        /// <returns>Coordonnées en byte du meilleur placement</returns>
        private static byte[] CalculMeilleurPlacement() => MaxPointListHori > MaxPointListVerti
                ? CoordPointHori((byte)ListHori.IndexOf(MaxPointListHori))
                : CoordPointVerti((byte)ListVerti.IndexOf(MaxPointListVerti));

        /// <summary>
        /// Veuillez Initialisez la classe avec InitClass avant d'appeler cette propriétée.
        /// Permet d'obtenir le meilleur placement pour 1 tour pour un joueur donné
        /// </summary>
        /// <param name="p_player">Joueur testé</param>
        /// <returns>Coordonnées en byte du prochain placement</returns>
        public static byte[] MeilleurPlacement()
        {
            ComptaPointPartie.Comptabilisation(m_carte, m_joueur, m_adversaire);
            m_ptsAdv = m_adversaire.Points;
            return CalculMeilleurPlacement();
        }
    }
}