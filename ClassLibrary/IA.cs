﻿using ClassLibrary.IAObject;

namespace ClassLibrary
{
    public static class IA
    {
        public static byte[] ActionTour(Carte p_carte, Player p_player, Player p_player2)
        {
            CalculateurDePoints.InitClass(p_carte.DernierePose, p_carte, p_player, p_player2);
            return CalculateurDePoints.MeilleurPlacement();
        }
    }
}