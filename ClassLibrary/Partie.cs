﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary
{
    public class Partie
    {
        public bool[,] MapBool { get; } = new bool[10, 10];
        public byte[,] MapRes { get; } = new byte[10, 10];
        public Carte MaCarte { get; } = new Carte();

        #region Initialisation de la classe

        /// <summary>
        /// Permet l'initialisation de mapRes.
        /// </summary>
        /// <param name="p_trame">string comportant toutes les valeurs encriptées d'unitée de la carte</param>
        private void InitialisationTerrainMapRes(string p_trame)
        {
            byte v_coordonneeX = 0, v_coordonneeY;
            foreach (string v_line in p_trame.Split('|'))
            {
                if (v_line != "")
                {
                    v_coordonneeY = 0;
                    foreach (string v_numStr in v_line.Split(':'))
                    {
                        byte.TryParse(v_numStr, out byte v_num);
                        MapRes[v_coordonneeX, v_coordonneeY] = v_num;
                        MapBool[v_coordonneeX, v_coordonneeY++] = true;
                    }
                }
                v_coordonneeX++;
            }
        }

        /// <summary>
        /// Initialise toute les case de la carte
        /// </summary>
        private void InitialisationTerrainCarte()
        {
            for (byte v_coordonneeX = 0; v_coordonneeX < 10; v_coordonneeX++)
                for (byte v_coordonneeY = 0; v_coordonneeY < 10; v_coordonneeY++)
                {
                    Unite v_nouvelleUnite = new Unite(v_coordonneeX, v_coordonneeY);
                    TradTypeUnite(MapRes[v_coordonneeX, v_coordonneeY], v_nouvelleUnite);
                    MaCarte.AjoutUnite(v_nouvelleUnite, v_coordonneeX, v_coordonneeY);
                }
        }

        /// <summary>
        /// Permet l'initialisation de la carte.
        /// </summary>
        /// <param name="p_trame">string comportant toutes les valeurs encriptées d'unitée de la carte</param>
        private void InitialisationTerrain(string p_trame)
        {
            InitialisationTerrainMapRes(p_trame);
            InitialisationTerrainCarte();
        }

        /// <summary>
        /// Permet l'initialisation de toutes les parcelles du terrain.
        /// La carte et ses unitées doivent être déjà initialisée
        /// </summary>
        private void InitialisationParcelle()
        {
            byte v_numeroParcelle = 0; // Initialisée à 0 pour simplifier la récupération de la parcelle dans la liste de parcelle
            for (byte v_coordonneeX = 0; v_coordonneeX < 10; v_coordonneeX++)
                for (byte v_coordonneeY = 0; v_coordonneeY < 10; v_coordonneeY++)
                { // Double boucle for permettant de regarder tout le terrain. (10/10)
                    List<Unite> v_listeAModifier = new List<Unite>(); // Initialisation d'une liste d'unités
                    RechercheFrontiere(v_coordonneeX, v_coordonneeY, v_listeAModifier); // Permet de remplir la liste d'unités
                    if (v_listeAModifier.Count() != 0)
                    { // Si la liste n'est pas vide alors :
                        Parcelle v_nouvelleParcelle = new Parcelle(v_listeAModifier, v_numeroParcelle++); // Permet l'ajout de la liste dans les informations de la carte.
                        foreach (Unite v_element in v_nouvelleParcelle.Unites)
                            v_element.Parcelle = v_nouvelleParcelle;
                        MaCarte.AddParcelle(v_nouvelleParcelle);
                    }
                }
        }

        /// <summary>
        /// Fonction permettant l'initialisation de la partie. Doit être appeler en début de programme
        /// </summary>
        /// <param name="p_resTrame">string comportant toutes les valeurs encriptées d'unitée de la carte</param>
		public Partie(string p_resTrame)
        {
            InitialisationTerrain(p_resTrame);
            InitialisationParcelle();
        }

        #endregion Initialisation de la classe

        #region Code de création de terrain

        /// <summary>
        /// Permet de décoder le type de Unite : Océan, Champ, Forêt. Et d'ensuite décoder ses frontières.
        /// </summary>
        /// <param name="p_nbUnite">Chiffre a décoder</param>
        /// <param name="p_unite">Unitée actuellement modifier</param>
		private void TradTypeUnite(byte p_nbUnite, Unite p_unite)
        {
            //Set le type d'unitée
            p_unite.Type = p_nbUnite >= 64 ? 'M' : p_nbUnite >= 32 ? 'F' : 'T';
            //Set les frontières
            TradFrontiere(p_nbUnite, p_unite);
        }

        /// <summary>
        /// Permet de décoder les frontières d'une Unitée
        /// </summary>
        /// <param name="p_nbUnite">Chiffre a décoder</param>
        /// <param name="p_unite">Unitée actuellement modifier</param>
		private void TradFrontiere(byte p_nbUnite, Unite p_unite)
        {
            p_unite.SetFrontieres(1, p_nbUnite % 16 >= 8); //frontière Est
            p_unite.SetFrontieres(2, p_nbUnite % 8 >= 4); //frontière Sud
            p_unite.SetFrontieres(3, p_nbUnite % 4 >= 2); //frontière Ouest
            p_unite.SetFrontieres(0, p_nbUnite % 2 == 1); //frontière Nord
        }

        #endregion Code de création de terrain

        #region Code de recherche de Frontière

        /// <summary>
        /// Fonction permettant de faire les vérification de création de parcelle de manière Vertical
        /// Soit rien ne ce passe ou alors récursive sur la fonction Recherche Frontière
        /// </summary>
        /// <param name="p_coordonneeX">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_coordonneeY">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_listeAModifier">liste de toutes les unitées</param>
        /// <param name="p_decalagePosition">1 ou -1 pour diriger le déplacement de la vérification</param>
        private void RechercheFrontiereVertical(byte p_coordonneeX, byte p_coordonneeY, List<Unite> p_listeAModifier, short p_decalagePosition)
        {
            if (MaCarte.GetUnite(p_coordonneeX, p_coordonneeY).GetFrontieres((p_decalagePosition == -1) ? (byte)0 : (byte)2) == false) //Permet de vérifier si une frontière est présent avant d'executer la suite du code
                if (MapBool[p_coordonneeX + p_decalagePosition, p_coordonneeY]) //Permet de vérifier si la position à déjà était tester
                    RechercheFrontiere((byte)(p_coordonneeX + p_decalagePosition), p_coordonneeY, p_listeAModifier); //Permet de faire une récursion sur la nouvelle position
        }

        /// <summary>
        /// Permets le déplacement du curseur la case suivante, vérifie s'il n'y a
        /// pas de frontière et que les coordonnées suivante ne sont pas vérifier
        /// </summary>
        /// <param name="p_coordonneeX">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_coordonneeY">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_listeAModifier">liste de toutes les unitées</param>
        /// <param name="p_decalagePosition">1 ou -1 pour diriger le déplacement de la vérification</param>
        private void RechercheFrontiereHorizontal(byte p_coordonneeX, byte p_coordonneeY, List<Unite> p_listeAModifier, short p_decalagePosition)
        {
            if (MaCarte.GetUnite(p_coordonneeX, p_coordonneeY).GetFrontieres((p_decalagePosition == 1) ? (byte)1 : (byte)3) == false) //Permet de vérifier si une frontière est présent avant d'executer la suite du code
                if (MapBool[p_coordonneeX, p_coordonneeY + p_decalagePosition]) //Permet de vérifier si la position à déjà était tester
                    RechercheFrontiere(p_coordonneeX, (byte)(p_coordonneeY + p_decalagePosition), p_listeAModifier); //Permet de faire une récursion sur la nouvelle position
        }

        /// <summary>
        /// Fait appel aux fonctions RechercheFrontiere pour chaque direction (Nord, Sud...)
        /// </summary>
        /// <param name="p_coordonneeX">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_coordonneeY">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_listeAModifier">liste de toutes les unitées</param>
        private void RechercheFrontiereMultiDirectionelle(byte p_coordonneeX, byte p_coordonneeY, List<Unite> p_listeAModifier)
        {
            RechercheFrontiereHorizontal(p_coordonneeX, p_coordonneeY, p_listeAModifier, 1);  //permet de récuperer la frontière est
            RechercheFrontiereHorizontal(p_coordonneeX, p_coordonneeY, p_listeAModifier, -1); //permet de récuperer la frontière ouest
            RechercheFrontiereVertical(p_coordonneeX, p_coordonneeY, p_listeAModifier, 1);    //permet de récuperer la frontière sud
            RechercheFrontiereVertical(p_coordonneeX, p_coordonneeY, p_listeAModifier, -1);   //permet de récuperer la frontière nord
        }

        /// <summary>
        /// Vérification de si la case n'a pas été vérifier et n'est pas un océan ou une forêt
        /// </summary>
        /// <param name="p_coordonneeX">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_coordonneeY">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <returns>boolean true si condition OK sinon false</returns>
        private bool VerificationUniteAcceptable(byte p_coordonneeX, byte p_coordonneeY) =>
            MapBool[p_coordonneeX, p_coordonneeY] ? MaCarte.GetUnite(p_coordonneeX, p_coordonneeY).IsTerre : false;

        /// <summary>
        /// Permets le renvoi d'une parcelle en cherchant sur la carte depuis une coordonnée fournie
        /// </summary>
        /// <param name="p_coordonneeX">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_coordonneeY">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_listeAModifier">Liste Vide à fournir</param>
        private void RechercheFrontiere(byte p_coordonneeX, byte p_coordonneeY, List<Unite> p_listeAModifier)
        {
            if (VerificationUniteAcceptable(p_coordonneeX, p_coordonneeY))//permet de vérifier si cette position peut être vérifiée
            {
                p_listeAModifier.Add(MaCarte.GetUnite(p_coordonneeX, p_coordonneeY)); //ajoute une des unite a la liste de la parcelle
                MapBool[p_coordonneeX, p_coordonneeY] = false; //permet d'éviter de utiliser la case une seconde fois
                RechercheFrontiereMultiDirectionelle(p_coordonneeX, p_coordonneeY, p_listeAModifier); //commande permettant de passer aux cases suivantes
            }
            MapBool[p_coordonneeX, p_coordonneeY] = false;
        }

        #endregion Code de recherche de Frontière

        #region Code d'un Tour de partie
        /// <summary>
        /// Permet de décoder une action de string en tableau de byte
        /// </summary>
        /// <param name="coup">string de type A:XY</param>
        /// <returns>Tableau de byte contenant les nouvelles coordonnées</returns>
        private byte[] SplitPose(string coup) => new byte[] { byte.Parse(coup.Split(':')[1].Substring(0, 1)), byte.Parse(coup.Split(':')[1].Substring(1, 1)) };

        /// <summary>
        /// Permet de d'encoder une action de tableau de byte en string
        /// </summary>
        /// <param name="p_lettreJoueur">Lettre du Joueur allant jouer</param>
        /// <param name="p_coord">Tableau de byte possèdant les coordonnées de pose</param>
        /// <returns>String encodé contenant les coordonnées de la future pose</returns>
        public string ConcatenationActionJoueur(char p_lettreJoueur, byte[] p_coord) => string.Format("{0}:{1}{2}", p_lettreJoueur, p_coord[0], p_coord[1]);

        /// <summary>
        /// Calcul de la meilleur position de pose pour le premier tour et initialise la partie
        /// </summary>
        /// <param name="p_carte">Carte Actuelle</param>
        /// <param name="p_communications">Interface de communication</param>
        /// <param name="p_iaPlayer">Joueur du Client</param>
        /// <param name="p_servPlayer">Joueur du Serveur</param>
        public void PremierTour(ref Carte p_carte, ref Communications p_communications, ref Player p_iaPlayer, ref Player p_servPlayer)
        {
            Unite v_unite = p_carte.MeilleurParcelle.Unites[0];
            string newCoupJoueur = ConcatenationActionJoueur('A', new byte[] { v_unite.CoordonneeX, v_unite.CoordonneeY });
            Tour(newCoupJoueur, ref p_carte, ref p_communications, ref p_iaPlayer, ref p_servPlayer);
        }

        /// <summary>
        /// Permet le déroulement de la partie du début à la fin
        /// </summary>
        /// <param name="p_coupJ1">Première action de la partie</param>
        /// <param name="p_carte">Carte Actuelle</param>
        /// <param name="p_communications">Interface de communication</param>
        /// <param name="p_iaPlayer">Joueur du client</param>
        /// <param name="p_servPlayer">Joueur du serveur</param>
        private void Tour(string p_coupJ1, ref Carte p_carte, ref Communications p_communications, ref Player p_iaPlayer, ref Player p_servPlayer)
        {
            // -----------------------------A:XY---------------------------------->
            p_communications.SendInfo(p_coupJ1, p_communications.SocketPartie);

            // <------------------------VALI OU INVA-------------------------------
            string m_retour = p_communications.ReceiveInfo(p_communications.SocketPartie);
            if (m_retour == "VALI")
            {
                byte[] coupIA = SplitPose(p_coupJ1);
                p_iaPlayer.PosePion(coupIA[0], coupIA[1], p_carte);
                p_carte.DernierePose = coupIA;
            }

            // <------------------------B:XY OU FINI-------------------------------
            m_retour = p_communications.ReceiveInfo(p_communications.SocketPartie);
            if (m_retour != "FINI")
            {
                byte[] coupServ = SplitPose(m_retour);
                p_servPlayer.PosePion(coupServ[0], coupServ[1], p_carte);
                p_carte.DernierePose = coupServ;
            }
            else { return; }

            // <------------------------ENCO OU FINI-------------------------------
            m_retour = p_communications.ReceiveInfo(p_communications.SocketPartie);
            if (m_retour != "FINI")
            {
                // OPTIONNEL: comptage points IA & SERV
                string newCoupJoueur = ConcatenationActionJoueur('A', IA.ActionTour(p_carte, p_iaPlayer, p_servPlayer));
                Tour(newCoupJoueur, ref p_carte, ref p_communications, ref p_iaPlayer, ref p_servPlayer);
            }
        }

        #endregion Code d'un Tour de partie
    }
}