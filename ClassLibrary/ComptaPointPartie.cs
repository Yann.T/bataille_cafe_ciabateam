﻿using System.Collections.Generic;

namespace ClassLibrary
{
    public static class ComptaPointPartie
    {
        #region RechercheTailleTerrain

        /// <summary>
        /// Vérifie si les coordonnées fournies sont possibles, et si la personne qui contrôle cette case n'est pas la même
        /// </summary>
        /// <param name="p_cartes">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_mapBool">Tableau permettant de savoir quelle unité a déjà été vérifiée</param>
        /// <param name="p_idJoueur">Permet une vérification de contrôle de case</param>
        /// <returns>retourne si oui ou non, la case est utilisable</returns>
        private static bool VerificationUniteAcceptable(Unite[,] p_cartes, byte p_indiceHorizontal, byte p_indiceVertical, bool[,] p_mapBool, byte p_idJoueur)
        {
            if (p_indiceHorizontal >= 10 || p_indiceVertical >= 10) return false;
            if (p_mapBool[p_indiceHorizontal, p_indiceVertical]) return false;
            if (p_cartes[p_indiceHorizontal, p_indiceVertical].Owner == null) return false;
            return p_cartes[p_indiceHorizontal, p_indiceVertical].Owner.Id == p_idJoueur;
        }

        /// <summary>
        /// Si les conditions de vérification sont ok, alors réitération sur la fonction RechercheTailleTerrain, sinon renvois 0
        /// </summary>
        /// <param name="p_cartes">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_mapBool">Tableau permettant de savoir quelle unité a déjà été vérifiée</param>
        /// <param name="p_idJoueur">Permet une vérification de contrôle de case</param>
        /// <returns>Renvoie soit 0, soit un byte du résultat de la case suivante</returns>
        private static byte Recherche(Unite[,] p_cartes, byte p_indiceHorizontal, byte p_indiceVertical, bool[,] p_mapBool, byte p_idJoueur)
        {
            return !VerificationUniteAcceptable(p_cartes, p_indiceHorizontal, p_indiceVertical, p_mapBool, p_idJoueur) ? (byte)0
                : RechercheTailleTerrain(p_cartes, p_indiceHorizontal, p_indiceVertical, p_mapBool); //Permet de faire une récursion sur la nouvelle position
        }

        /// <summary>
        /// Fait appel à la fonction Recherche pour chaque directions (Nord, Sud...)
        /// </summary>
        /// <param name="p_cartes">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_mapBool">Tableau permettant de savoir quelle unité a déjà été vérifiée</param>
        /// <param name="p_idJoueur">Permet une vérification de contrôle de case</param>
        /// <returns>Renvoie un byte résultant de la somme de tout les résultats de la fonction Recherche</returns>
        private static byte RechercheMultiDirectionelle(Unite[,] p_cartes, byte p_indiceHorizontal, byte p_indiceVertical, bool[,] p_mapBool, byte p_idJoueur)
        {
            byte v_valeurCaseEst = Recherche(p_cartes, (byte)(p_indiceHorizontal + 1), p_indiceVertical, p_mapBool, p_idJoueur);  //permet de récuperer la frontière est
            byte v_valeurCaseOuest = Recherche(p_cartes, (byte)(p_indiceHorizontal - 1), p_indiceVertical, p_mapBool, p_idJoueur); //permet de récuperer la frontière ouest
            byte v_valeurCaseSud = Recherche(p_cartes, p_indiceHorizontal, (byte)(p_indiceVertical + 1), p_mapBool, p_idJoueur);    //permet de récuperer la frontière sud
            byte v_valeurCaseNord = Recherche(p_cartes, p_indiceHorizontal, (byte)(p_indiceVertical - 1), p_mapBool, p_idJoueur);   //permet de récuperer la frontière nord
            return (byte)(v_valeurCaseEst + v_valeurCaseNord + v_valeurCaseOuest + v_valeurCaseSud);
        }

        /// <summary>
        /// Permet depuis une coordonnée, de calculer la taille maximum d'un groupe de graine
        /// </summary>
        /// <param name="p_cartes">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_mapBool">Tableau permettant de savoir quelle unité a déjà été vérifiée</param>
        /// <returns>byte : Taille de la Parcelle</returns>
        public static byte RechercheTailleTerrain(Unite[,] p_cartes, byte p_indiceHorizontal, byte p_indiceVertical, bool[,] p_mapBool)
        {
            if (p_cartes[p_indiceHorizontal, p_indiceVertical].Owner != null)
            {
                byte p_idJoueur = p_cartes[p_indiceHorizontal, p_indiceVertical].Owner.Id;
                if (VerificationUniteAcceptable(p_cartes, p_indiceHorizontal, p_indiceVertical, p_mapBool, p_idJoueur))
                {
                    p_mapBool[p_indiceHorizontal, p_indiceVertical] = true; //permet d'éviter de utiliser la case une seconde fois
                    return (byte)(1 + RechercheMultiDirectionelle(p_cartes, p_indiceHorizontal, p_indiceVertical, p_mapBool, p_idJoueur));
                }
            }
            return 0;
        }

        #endregion RechercheTailleTerrain

        #region ComptabilisationMethodeUne

        /// <summary>
        /// Permet de compter tout les points d'un joueur en regardant toutes les parcelles lui appartenant.
        /// </summary>
        /// <param name="p_parcelles">liste de toutes les parcelles de la partie</param>
        /// <param name="p_playerUn">Référence vers le joueur principal</param>
        /// <param name="p_playerDeux">Référence vers le juouer adversaire</param>
        private static void ComptabilisationMethodeUne(List<Parcelle> p_parcelles, Player p_playerUn, Player p_playerDeux)
        {
            byte v_pointsJoueurUn = 0, v_pointsJoueurDeux = 0;
            foreach (Parcelle v_parcelle in p_parcelles)
                if (v_parcelle.Occupied)
                    if (v_parcelle.Owner.Id == 1)
                        v_pointsJoueurUn += v_parcelle.SizeParcelle;
                    else
                        v_pointsJoueurDeux += v_parcelle.SizeParcelle;
            p_playerUn.Points = v_pointsJoueurUn;
            p_playerDeux.Points = v_pointsJoueurDeux;
        }

        #endregion ComptabilisationMethodeUne

        #region ComptabilisationMethodeDeux

        /// <summary>
        /// Permet l'initialisation de la carte de Booléen.
        /// </summary>
        /// <returns>tableau de booléen</returns>
        public static bool[,] InitialisationCarteBool()
        {
            bool[,] v_mapBool = new bool[10, 10];
            for (byte v_indiceHorizontal = 0; v_indiceHorizontal < 10; v_indiceHorizontal++)
                for (byte v_indiceVertical = 0; v_indiceVertical < 10; v_indiceVertical++)
                    v_mapBool[v_indiceHorizontal, v_indiceVertical] = false;
            return v_mapBool;
        }

        /// <summary>
        /// Permet de calculer la taille maximum de la parcelle pour chaque joueurs et d'ensuite ajouter ces points dans le score des joueurs.
        /// </summary>
        /// <param name="p_cartes">La carte actuelle de la partie</param>
        /// <param name="p_playerUn">Référence vers le joueur principal</param>
        /// <param name="p_playerDeux">Référence vers le juouer adversaire</param>
        private static void ComptabilisationMethodeDeux(Unite[,] p_cartes, Player p_playerUn, Player p_playerDeux)
        {
            byte v_nombreCelluleMaxJoueurUn = 0, v_nombreCelluleMaxJoueurDeux = 0;
            bool[,] v_mapBool = InitialisationCarteBool();
            for (byte v_indiceHorizontal = 0; v_indiceHorizontal < 10; v_indiceHorizontal++)
                for (byte v_indiceVertical = 0; v_indiceVertical < 10; v_indiceVertical++)
                    if (p_cartes[v_indiceHorizontal, v_indiceVertical].Occupied)
                    {
                        byte v_resultatCase = RechercheTailleTerrain(p_cartes, v_indiceHorizontal, v_indiceVertical, v_mapBool);
                        if (p_cartes[v_indiceHorizontal, v_indiceVertical].Owner.Id == 1 && v_resultatCase > v_nombreCelluleMaxJoueurUn)
                            v_nombreCelluleMaxJoueurUn = v_resultatCase;
                        if (p_cartes[v_indiceHorizontal, v_indiceVertical].Owner.Id == 2 && v_resultatCase > v_nombreCelluleMaxJoueurDeux)
                            v_nombreCelluleMaxJoueurDeux = v_resultatCase;
                    }
            p_playerUn.Points = (byte)(p_playerUn.Points + v_nombreCelluleMaxJoueurUn);
            p_playerDeux.Points = (byte)(p_playerDeux.Points + v_nombreCelluleMaxJoueurDeux);
        }

        #endregion ComptabilisationMethodeDeux

        /// <summary>
        /// Permet de compté tout les points de chaque joueur. En utilisant les règles fournies par le jeux.
        /// Que ce soit par contrôle de territoire, ou part contrôle de la plus grande parcelle de graines.
        /// </summary>
        /// <param name="p_carte">Référence vers l'objet carte de la partie en cour</param>
        /// <param name="p_playerUn">Référence vers le joueur principal</param>
        /// <param name="p_playerDeux">Référence vers le juouer adversaire</param>
        public static void Comptabilisation(Carte p_carte, Player p_playerUn, Player p_playerDeux)
        {
            ComptabilisationMethodeUne(p_carte.Parcelles, p_playerUn, p_playerDeux);
            ComptabilisationMethodeDeux(p_carte.Unites, p_playerUn, p_playerDeux);
        }
    }
}